package com.founder.service;

import com.alibaba.fastjson.JSONObject;
import com.founder.config.XxlJobConfig;
import com.founder.core.utils.PayDigestUtil;
import com.founder.core.utils.PayUtil;
import com.founder.fiance.FianceBASE;
import com.founder.fiance.FianceFNYY;
import com.founder.fiance.FianceTJ3ZX;
import com.founder.utils.ApplicationContextUtil;
import com.xxl.job.core.log.XxlJobLogger;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class BaseService {

    @Autowired
    XxlJobConfig jobConfig;

    @Autowired
    FianceFNYY fianceFNYY;

    @Autowired
    FianceTJ3ZX fianceTJ3ZX;

    /**
     * 下载微信支付宝对账单
     * @param mch
     * @param date
     * @param channelId
     * @return
     */
    public String downloadBill(String mch,String date, String channelId){

        XxlJobLogger.log("###### 开始接收商户" + mch + "下载对账单请求" + channelId + " ######");

        JSONObject paramMap = new JSONObject();
        paramMap.put("mchId", mch);                               // 商户ID
        paramMap.put("channelId", channelId);
        paramMap.put("date", date);

        String reqKey = jobConfig.getReq_key();
        String baseUrl = jobConfig.getBase_url();

        String reqSign = PayDigestUtil.getSign(paramMap, reqKey);
        paramMap.put("sign", reqSign);                              // 签名
        String reqData = "params=" + paramMap.toJSONString();
        XxlJobLogger.log("请求支付中心下载对账单接口,请求数据:" + reqData);
        String url = baseUrl + "/query/download_bill?";
        String result = PayUtil.call4Post(url + reqData);
        XxlJobLogger.log("请求支付中心下载对账单接口,响应数据:" + result);

        return result;
    }

    /**
     * 生成对账内容
     * @param list
     * @return
     */
    public String buildContent(List<?> list, String subsys, String hostipal_code) {
        XxlJobLogger.log("根据医院编码生成对账");
        String bean = "fiance" + hostipal_code;
        XxlJobLogger.log("调用" + bean + "服务");
        FianceBASE fiance = (FianceBASE) ApplicationContextUtil.getBean(bean);
        if (fiance != null){
            return fiance.buildContent(list, subsys);
        }
        return null;
    }
}
