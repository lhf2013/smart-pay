package com.founder.core.base;

import java.io.Serializable;

public class BaseFieldInfo implements Serializable {
    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldKind() {
        return fieldKind;
    }

    public void setFieldKind(String fieldKind) {
        this.fieldKind = fieldKind;
    }

    public Integer getFieldLength() {
        return fieldLength;
    }

    public void setFieldLength(Integer fieldLength) {
        this.fieldLength = fieldLength;
    }

    private String fieldName;
    private String fieldKind;
    private Integer fieldLength;
}
