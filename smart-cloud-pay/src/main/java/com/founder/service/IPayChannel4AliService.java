package com.founder.service;

import com.founder.core.domain.PayOrder;
import com.founder.core.domain.RefundOrder;

public interface IPayChannel4AliService {

    /**
     * 条码支付
     * @param channelId
     * @param payOrder
     * @return
     */
    String doAliPayReq(String channelId, PayOrder payOrder);

    /**
     * 扫码支付
     * @param channelId
     * @param payOrder
     * @return
     */
    String doAliPrePayReq(String channelId, PayOrder payOrder);

    /**
     * 订单查询
     * @param channelId
     * @param payOrder
     * @return
     */
    String doAliQueryReq(String channelId, PayOrder payOrder);

    /**
     * 订单退费
     * @param channelId
     * @param refundOrder
     * @return
     */
    String doAliRefundReq(String channelId, RefundOrder refundOrder);

    /**
     * 订单撤销
     * @param channelId
     * @param refundOrder
     * @return
     */
    String doAliCancelReq(String channelId, RefundOrder refundOrder);

    String doAliBillReq(String mchId, String channelId, String billDate);
}
