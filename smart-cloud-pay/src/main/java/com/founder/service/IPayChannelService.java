package com.founder.service;

import com.founder.core.domain.PayChannel;

public interface IPayChannelService {

    PayChannel selectPayChannel(String channelId, String mchId);
}
